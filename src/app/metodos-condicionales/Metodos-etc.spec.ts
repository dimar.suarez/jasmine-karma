import { MetodosCondicionales } from "./Metodos-etc"

describe('Test Metodos condicionales', () => {
    let component: MetodosCondicionales;

    beforeEach(() => {
        component = new MetodosCondicionales();
    })

    it('Probando variable 1 verdadero', () => {
        expect(component.probandoCondicionales()).toBe('variable 1 verdadero');
    });

    it('Probando variable 1 falsa', () => {
        component.var1 = false
        expect(component.probandoCondicionales()).toBe('variable 2 y 3 verdaderas');
    });

    it('Probando variable 1 y 3 falsa', () => {
        component.var1 = false
        component.var3 = false
        expect(component.probandoCondicionales()).toBe('variable 2 es verdadera');
    });

    it('Probando variable 1 y 3 falsa', () => {
        component.var1 = false
        component.var2 = false
        component.var3 = false
        expect(component.probandoCondicionales()).toBe('todas son falsas');
    });
})