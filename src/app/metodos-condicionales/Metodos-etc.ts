
export class MetodosCondicionales {

  public var1: boolean = true;
  public var2: boolean = true;
  public var3: boolean = true;

  constructor() {
  }

  public probandoCondicionales(): string {
    if (this.var1){
      return 'variable 1 verdadero';
    }else if(this.var2){
      if(this.var3){
        return 'variable 2 y 3 verdaderas';
      }
      return 'variable 2 es verdadera';
    }
    return 'todas son falsas';
  }
}
