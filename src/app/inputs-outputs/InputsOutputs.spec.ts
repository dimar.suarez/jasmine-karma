import { InputOutputsComponent } from "./InputsOutputs"

describe('Test Inputs Outputs', () => {
    let component: InputOutputsComponent;

    beforeEach(() => {
        component = new InputOutputsComponent();
    })

    it('Prueba Input', () => {
        expect(component.entrada).toBe('default');
        component.borrarEntrada();
        expect(component.entrada).toBe('');
    })

    it('Prueba Iutput', () => {
        const ARG: string = 'emitiendo';
        component.emitirAlgo(ARG);
        component.salida.subscribe(value => {
            expect(value).toBe(ARG);
        });
    })
})