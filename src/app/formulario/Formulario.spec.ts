import { FormBuilder } from "@angular/forms"
import { unFormulario } from "./Formulario"

describe('Prueba Formulario', () => {
    const component = new unFormulario(new FormBuilder());

    it('Form contains email', () => {
        expect(component.form.contains('email')).toBeTruthy();
    })

    it('Form contains password', () => {
        expect(component.form.contains('password')).toBeTruthy();
    })

    it('Email required', () => {
        const control = component.form.get('email');
        control?.setValue('')
        expect(control?.valid).toBeFalsy();
    })

    it('Email validator false', () => {
        const control = component.form.get('email');
        control?.setValue('pedro')
        expect(control?.valid).toBeFalsy();
    })

    it('Email validator true', () => {
        const control = component.form.get('email');
        control?.setValue('pedro@sofka.com')
        expect(control?.valid).toBeTruthy();
    })

    it('Password required', () => {
        const control = component.form.get('password');
        control?.setValue('')
        expect(control?.valid).toBeFalsy();
    })
})