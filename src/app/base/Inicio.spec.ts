import { primeros } from "./Inicio"

describe('Probando Inicio', () => {
    it(`El return debe ser 'Hola que hace'`, () => {
        const result = primeros();
        expect(result).toEqual('Hola que hace');
    })
})